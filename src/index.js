import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

const app = document.getElementById("page-wrap");
let appState = {};

window.setState = (stateChange) => {
  appState = Object.assign({}, appState, stateChange);

  ReactDOM.render(<App {...appState} />, app);
};

/* eslint no-restricted-globals: 0*/
let initialState = {
  urlLocation: location.pathname.replace(/^\/?|\/$/g, ""),
};

window.setState(initialState);
