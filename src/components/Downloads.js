import React, { Component } from "react";
import axios from "axios";
import "./styles.css";

import { Multiselect } from "multiselect-react-dropdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronCircleRight,
  faChevronCircleLeft,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";

const timeout = 2000;
const maxRowsInDownload = 2500000;

let lib = require("../utils/library");
let json2csv = require("json2csv");
var js2xmlparser = require("js2xmlparser");

export default class Downloads extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table: props.table,
      columns: props.columns,
      data: [],
      dataFull: [],
      url: props.url,
      fileFormat: "delimited",
      delimiterChoice: ";",
      columnChosen: 0,
      tableHeader: true,
      batchDownloadCheckBox: false,
      copyUniqueValuesOnlyToggle: false,
      fileNameCustom: "",
      reRunQuery: false,
      fileNameAuto: "",
      anchorEl: undefined,
      open: false,
      copyLoading: false,
      copyResult: "",
      batchSize: "100K",
      batchDownloadLowerNum: 0,
      batchDownloadUpperNum: 100000,
      snackBarVisibility: false,
      snackBarMessage: "Unknown error occured",
    };

    this.handleDelimiterChange = this.handleDelimiterChange.bind(this);
    this.handlebatchDownloadCheckBox = this.handlebatchDownloadCheckBox.bind(
      this
    );
    this.handleCopyUniqueValuesOnlyToggle = this.handleCopyUniqueValuesOnlyToggle.bind(
      this
    );
    this.handleLeftButtonClickRangeDownload = this.handleLeftButtonClickRangeDownload.bind(
      this
    );
    this.handleRightButtonClickRangeDownload = this.handleRightButtonClickRangeDownload.bind(
      this
    );
    this.handleTableHeaderToggle = this.handleTableHeaderToggle.bind(this);
    this.handleFileNameChange = this.handleFileNameChange.bind(this);
    this.handleCopyOutputClick = this.handleCopyOutputClick.bind(this);
    this.handleDownloadClick = this.handleDownloadClick.bind(this);
    this.handleCopyClick = this.handleCopyClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
    this.handlebatchDownloadChange = this.handlebatchDownloadChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    this.setState(
      {
        table: newProps.table,
        columns: newProps.columns,
        url: newProps.url,
        data: newProps.data,
        fileNameCustom: "",
        dataFull: [],
        columnChosen: 0,
      },
      () => {
        this.createFileName();
      }
    );
  }

  downloadFile(data, fileName, mimeType) {
    if (this.state.fileNameCustom === "") {
      window.download(data, fileName, mimeType);
    } else {
      window.download(data, this.state.fileNameCustom, mimeType);
    }
  }

  createFileName(dataFullStatus = false) {
    // Parse out the delimiter
    let delimiter = this.state.delimiterChoice.replace(/\\t/g, "\t"); // for tabs

    // Create a good file name for the file so user knows what the data in the file is all about
    /* EXPLANATIONS FOR THE REGEXES
            let fileName = this.state.url.replace(lib.getDbConfig(this.props.dbIndex, "url") + "/", "") // Get rid of the URL
                .replace("?", "-") /////// The params q-mark can be replaced with dash
                .replace(/&/g, '-') /////// All URL key-value separating ampersands can be replaced with dashes
                .replace(/=/g, '-') /////// Get rid of the = in favour of the -
                .replace(/\([^()]{15,}\)/g, "(vals)") /////// Replaces any single non-nested AND/OR conditions with (vals) if they're longer than 15 chars
                .replace(/\(([^()]|\(vals\)){50,}\)/g,"(nested-vals)") /////// Replaces any AND/OR conds with a single (vals) if it's longer than 50 chars
                .replace(/[.]([\w,"\s]{30,})[,]/g, "(in-vals)"); /////// Specifically targets the IN operator's comma separated vals .. replace if longer than 30 chars
            */
    let fileName = this.state.url
      .replace(lib.getDbConfig(this.props.dbIndex, "url") + "/", "")
      .replace("?", "-")
      .replace(/&/g, "-")
      .replace(/=/g, "-")
      .replace(/\([^()]{15,}\)/g, "(vals)")
      .replace(/\(([^()]|\(vals\)){50,}\)/g, "(nested-vals)")
      .replace(/[.]([\w,"\s]{30,})[,]/g, "(in-vals)");

    if (this.state.batchDownloadCheckBox === true || dataFullStatus === true) {
      fileName = fileName.replace(
        /limit-\d*/g,
        "limit-" +
          maxRowsInDownload +
          "-range-" +
          this.state.batchDownloadLowerNum +
          "-" +
          this.state.batchDownloadUpperNum
      );
    }

    if (this.state.fileFormat === "delimited") {
      if (delimiter === ",") {
        fileName += ".csv";
      } else if (delimiter === "\t") {
        fileName += ".tsv";
      } else {
        fileName += ".txt";
      }

      if (this.state.tableHeader === true) {
        fileName = fileName
          .replace(".csv", "-header.csv")
          .replace(".tsv", "-header.tsv")
          .replace(".txt", "-header.txt");
      }
    } else if (this.state.fileFormat === "xml") {
      fileName += ".xml";
    } else if (this.state.fileFormat === "json") {
      fileName += ".json";
    } else if (this.state.fileFormat === "fasta") {
      fileName += ".fasta";
    } else {
      fileName += ".txt";
    }

    this.setState({
      fileNameAuto: fileName,
    });

    return fileName;
  }

  downloadTableWithDelimiter(dataFullStatus = false) {
    if (dataFullStatus === false && JSON.stringify(this.state.data) !== "[]") {
      try {
        // Parse out the delimiter
        let delimiter = this.state.delimiterChoice.replace(/\\t/g, "\t"); // for tabs
        let result = json2csv.parse(this.state.data, {
          fields: this.state.columns,
          delimiter: delimiter,
          header: this.state.tableHeader,
        });
        let fileName = this.createFileName();

        this.downloadFile(result, fileName, "text/plain");
      } catch (err) {
        console.error(err);
      }
    } else if (dataFullStatus === true) {
      if (JSON.stringify(this.state.dataFull) !== "[]") {
        try {
          // Parse out the delimiter
          let delimiter = this.state.delimiterChoice.replace(/\\t/g, "\t"); // for tabs
          let result = json2csv.parse(this.state.data, {
            fields: this.state.columns,
            delimiter: delimiter,
            header: this.state.tableHeader,
          });
          let fileName = this.createFileName(true);

          this.downloadFile(result, fileName, "text/plain");
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  downloadTableAsJSON(dataFullStatus = false) {
    if (dataFullStatus === false && JSON.stringify(this.state.data) !== "[]") {
      try {
        let result = JSON.stringify(this.state.data);
        let fileName = this.createFileName();

        this.downloadFile(result, fileName, "text/plain");
      } catch (err) {
        console.error(err);
      }
    } else if (dataFullStatus === true) {
      if (JSON.stringify(this.state.dataFull) !== "[]") {
        try {
          let result = JSON.stringify(this.state.dataFull);
          let fileName = this.createFileName(true);

          this.downloadFile(result, fileName, "text/plain");
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  copyTableAsJSON(dataFullStatus = false) {
    if (dataFullStatus === false && JSON.stringify(this.state.data) !== "[]") {
      try {
        let result = JSON.stringify(this.state.data);
        this.setState({ copyResult: result });
        let copySuccess = this.insertToClipboard(result);
        if (copySuccess) {
          this.setState({ copyLoading: false });
        }
      } catch (err) {
        console.error(err);
      }
    } else if (dataFullStatus === true) {
      if (JSON.stringify(this.state.dataFull) !== "[]") {
        try {
          let result = JSON.stringify(this.state.dataFull);
          let copySuccess = this.insertToClipboard(result);
          if (copySuccess) {
            this.setState({ copyLoading: false });
          }
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  downloadTableAsXML(dataFullStatus = false) {
    if (dataFullStatus === false && JSON.stringify(this.state.data) !== "[]") {
      try {
        let result = js2xmlparser.parse(this.state.table, this.state.data);
        let fileName = this.createFileName();

        this.downloadFile(result, fileName, "text/plain");
      } catch (err) {
        console.error(err);
      }
    } else if (dataFullStatus === true) {
      if (JSON.stringify(this.state.dataFull) !== "[]") {
        try {
          let result = js2xmlparser.parse(
            this.state.table,
            this.state.dataFull
          );
          let fileName = this.createFileName(true);

          this.downloadFile(result, fileName, "text/plain");
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  copyTableAsXML(dataFullStatus = false) {
    if (dataFullStatus === false && JSON.stringify(this.state.data) !== "[]") {
      try {
        let result = js2xmlparser.parse(this.state.table, this.state.data);
        let copySuccess = this.insertToClipboard(result);
        if (copySuccess) {
          this.setState({ copyLoading: false });
        }
      } catch (err) {
        console.error(err);
      }
    } else if (dataFullStatus === true) {
      if (JSON.stringify(this.state.dataFull) !== "[]") {
        try {
          let result = js2xmlparser.parse(
            this.state.table,
            this.state.dataFull
          );
          let copySuccess = this.insertToClipboard(result);
          if (copySuccess) {
            this.setState({ copyLoading: false });
          }
        } catch (err) {
          console.error(err);
        }
      }
    }
  }

  identifySeqColumnInStateColumns() {
    let seqColumn = null;
    let seqColumnNames = lib.getValueFromConfig("seq_column_names");
    for (let i in this.state.columns) {
      let columnName = this.state.columns[i];

      if (lib.inArray(columnName, seqColumnNames)) {
        seqColumn = columnName;
        break;
      }
    }
    return seqColumn;
  }

  downloadTableAsFASTA(dataFullStatus = false) {
    let seqColumn = this.identifySeqColumnInStateColumns();

    // proceed if a sequence column was found, proceed w/ the first found column....
    if (seqColumn !== null) {
      if (
        dataFullStatus === false &&
        JSON.stringify(this.state.data) !== "[]"
      ) {
        // TO DO: DETECT Protein or nucleotide tables automatically by name
        try {
          let result = "";

          for (let index in this.state.data) {
            let element = this.state.data[index];
            let seq = element[seqColumn];

            // Parse header string ...
            let header = ">";
            for (let index in this.state.columns) {
              if (this.state.columns[index] !== seqColumn) {
                header += "|" + element[this.state.columns[index]];
              }
            }

            result += header.replace(">|", ">");
            result += "\n";
            result += seq;
            result += "\n";
          }

          let fileName = this.createFileName();
          this.downloadFile(result, fileName, "text/plain");
        } catch (err) {
          console.error(err);
        }
      } else if (dataFullStatus === true) {
        if (JSON.stringify(this.state.dataFull) !== "[]") {
          // TO DO: DETECT Protein or nucleotide tables automatically by name
          try {
            let result = "";

            for (let index in this.state.dataFull) {
              let element = this.state.dataFull[index];
              let seq = element[seqColumn];

              // Parse header string ...
              let header = ">";
              for (let index in this.state.columns) {
                if (this.state.columns[index] !== seqColumn) {
                  header += "|" + element[this.state.columns[index]];
                }
              }

              result += header.replace(">|", ">");
              result += "\n";
              result += seq;
              result += "\n";
            }

            let fileName = this.createFileName(true);
            this.downloadFile(result, fileName, "text/plain");
          } catch (err) {
            console.error(err);
          }
        }
      }
    } else {
      // User got here by mistake, reset Download.js options
      this.handleResetClick();
    }
  }

  insertToClipboard(str) {
    //based on https://stackoverflow.com/a/12693636
    document.oncopy = function (event) {
      event.clipboardData.setData("text/plain", str);
      event.preventDefault();
    };
    let copySuccess = document.execCommand("copy");
    document.oncopy = undefined;
    return copySuccess;
  }

  handleFileFormatChange = (event) => {
    console.log(event.target.value);
    if (event.target.value !== "delimiterInput") {
      this.setState({ fileFormat: event.target.value }, () => {
        this.createFileName();
        this.setState({ fileNameCustom: "" });
      });
    }
  };

  handleTableHeaderToggle() {
    this.setState(
      {
        tableHeader: !this.state.tableHeader,
      },
      () => {
        this.createFileName();
      }
    );
  }

  handlebatchDownloadCheckBox() {
    this.setState(
      {
        batchDownloadCheckBox: !this.state.batchDownloadCheckBox,
      },
      () => {
        this.createFileName();
      }
    );
  }

  handleCopyUniqueValuesOnlyToggle(event) {
    this.setState({
      copyUniqueValuesOnlyToggle: !this.state.copyUniqueValuesOnlyToggle,
    });
  }

  handlebatchDownloadChange(event, e) {
    this.setState(
      {
        batchSize: e,
        batchDownloadLowerNum: 0,
        batchDownloadUpperNum: parseInt(e.replace("K", ""), 10) * 1000,
      },
      () => {
        this.createFileName(true);
      }
    );
    event.preventDefault();
  }

  handleLeftButtonClickRangeDownload(event) {
    let range = parseInt(this.state.batchSize.replace("K", ""), 10) * 1000;
    if (this.state.batchDownloadLowerNum - range > this.props.totalRows) {
      this.setState(
        {
          batchDownloadLowerNum:
            Math.trunc(this.props.totalRows / range) * range,
          batchDownloadUpperNum: this.props.totalRows,
        },
        () => {
          this.createFileName(true);
        }
      );
    } else if (
      this.state.batchDownloadLowerNum > 0 &&
      this.state.batchDownloadLowerNum - range >= 0
    ) {
      this.setState(
        {
          batchDownloadLowerNum: this.state.batchDownloadLowerNum - range,
          batchDownloadUpperNum: this.state.batchDownloadLowerNum,
        },
        () => {
          this.createFileName(true);
        }
      );
    } else {
      this.setState(
        {
          batchDownloadLowerNum: 0,
          batchDownloadUpperNum: range,
        },
        () => {
          this.createFileName(true);
        }
      );
    }
    event.preventDefault();
  }

  handleRightButtonClickRangeDownload(event) {
    let range = parseInt(this.state.batchSize.replace("K", ""), 10) * 1000;
    if (
      this.props.totalRows &&
      this.state.batchDownloadLowerNum + range + range > this.props.totalRows
    ) {
      this.setState(
        {
          batchDownloadLowerNum:
            Math.trunc(this.props.totalRows / range) * range,
          batchDownloadUpperNum: this.props.totalRows,
        },
        () => {
          this.createFileName(true);
        }
      );
    } else {
      this.setState(
        {
          batchDownloadLowerNum: this.state.batchDownloadLowerNum + range,
          batchDownloadUpperNum:
            this.state.batchDownloadLowerNum + range + range,
        },
        () => {
          this.createFileName(true);
        }
      );
    }
    event.preventDefault();
  }

  handleDelimiterChange(event) {
    let newValue = event.target.value;

    if (newValue.length === 0) {
      this.setState({ delimiterChoice: ";" }, () => {
        this.createFileName();
        this.setState({ fileNameCustom: "" });
      });
    } else if (newValue.length <= 5) {
      this.setState({ delimiterChoice: newValue }, () => {
        this.createFileName();
        this.setState({ fileNameCustom: "" });
      });
    }
  }

  handleFileNameChange(event) {
    let newValue = event.target.value;
    this.setState({ fileNameCustom: newValue });
  }

  handleResetClick() {
    this.setState(
      {
        fileFormat: "delimited",
        delimiterChoice: ";",
        columnChosen: 0,
        tableHeader: true,
        batchDownloadCheckBox: false,
        copyUniqueValuesOnlyToggle: false,
        fileNameCustom: "",
        copyLoading: false,
        copyResult: "",
        batchSize: "100K",
        batchDownloadLowerNum: 0,
        batchDownloadUpperNum: 100000,
      },
      () => {
        this.createFileName();
      }
    );
  }

  handleCopyClick() {
    this.setState({ copyLoading: true }, () => {
      if (
        this.state.batchDownloadCheckBox === true &&
        this.state.fileFormat !== "delimitedColumn" &&
        1 === 0
      ) {
        // DISABLED FOR NOW
        let dataFullURL = this.state.url.replace(
          /limit=\d*/g,
          "limit=" + maxRowsInDownload
        );
        this.fetchOutput(dataFullURL);
      } else {
        if (this.state.fileFormat === "delimited") {
          //this.downloadTableWithDelimiter();
        } else if (this.state.fileFormat === "delimitedColumn") {
          // With threads
          /*
                              myWorker.postMessage({ method: 'delimitedColumn', column: this.state.columnChosen, data: this.state.data, columns: this.state.columns });
                              myWorker.onmessage = (m) => {
                                  this.setState({ copyLoading: false, copyResult: m.data });
                              };
                              */

          // Without threads
          let column = this.state.columnChosen;
          let data = this.state.data;
          let columns = this.state.columns;
          let uniqueOnly = this.state.copyUniqueValuesOnlyToggle;

          let output = "";

          if (uniqueOnly) {
            let uniqueValues = new Set();
            for (let i = 0; i < data.length; i++) {
              uniqueValues.add(data[i][columns[column]]);
            }

            for (let valueToCopy of uniqueValues.values()) {
              if (
                String(valueToCopy) &&
                String(valueToCopy).match(/[\W|\s]/g)
              ) {
                output += '"' + valueToCopy + '",';
              } else {
                output += valueToCopy + ",";
              }
            }
          } else {
            for (let i = 0; i < data.length; i++) {
              let valueToCopy = data[i][columns[column]];
              if (
                String(valueToCopy) &&
                String(valueToCopy).match(/[\W|\s]/g)
              ) {
                output += '"' + valueToCopy + '",';
              } else {
                output += valueToCopy + ",";
              }
            }
          }

          output = output.replace(/,$/g, "");

          let result = this.insertToClipboard(output);

          this.setState(
            {
              copyLoading: false,
              snackBarVisibility: true,
              snackBarMessage: result ? "Copied!" : "Error copying data ...",
            },
            () => {
              this.timer = setTimeout(() => {
                this.setState({
                  snackBarVisibility: false,
                  snackBarMessage: "Unknown error",
                });
              }, 2500);
            }
          );
        } else if (this.state.fileFormat === "json") {
          this.copyTableAsJSON();
          // myWorker.postMessage({method: 'json', data: this.state.data});
          // myWorker.onmessage = (m) => {
          //     this.setState({copyLoading: false, copyResult: m.data});
          // };
        } else if (this.state.fileFormat === "xml") {
          this.copyTableAsXML();
          // myWorker.postMessage({method: 'xml', data: this.state.data});
          // myWorker.onmessage = (m) => {
          //     this.setState({copyLoading: false, copyResult: m.data});
          // };
        } else if (this.state.fileFormat === "fasta") {
          //this.downloadTableAsFASTA();
        }

        this.setState({
          submitSuccess: true,
          submitError: false,
          submitLoading: false,
          dataFull: [],
        });
      }
    });
  }

  handleCopyOutputClick() {
    //
  }

  handleDownloadClick() {
    this.createFileName();

    this.setState(
      {
        submitLoading: true,
        submitSuccess: false,
        submitError: false,
        dataFull: [],
      },
      () => {
        if (this.state.batchDownloadCheckBox === true) {
          let dataFullURL = this.state.url.replace(
            /limit=\d*/g,
            "limit=" + maxRowsInDownload
          );
          this.fetchOutput(dataFullURL);
        } else {
          if (this.state.fileFormat === "delimited") {
            this.downloadTableWithDelimiter();
          } else if (this.state.fileFormat === "json") {
            this.downloadTableAsJSON();
          } else if (this.state.fileFormat === "xml") {
            this.downloadTableAsXML();
          } else if (this.state.fileFormat === "fasta") {
            this.downloadTableAsFASTA();
          }

          this.setState({
            submitSuccess: true,
            submitError: false,
            submitLoading: false,
            dataFull: [],
          });
        }
      }
    );
  }

  handleClickListItem = (event) => {
    this.setState({ open: true, anchorEl: event.currentTarget });
  };

  handleMenuItemClick(optionsList, selectedItem) {
    this.setState({
      columnChosen: this.state.columns.indexOf(selectedItem),
      open: false,
    });
  }

  // (event, index) => {
  //   this.setState({ columnChosen: index, open: false });
  // };

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  fetchOutput(url) {
    let preparedHeaders = {};
    if (this.state.batchDownloadCheckBox === true) {
      preparedHeaders = {
        Range:
          String(this.state.batchDownloadLowerNum) +
          "-" +
          String(this.state.batchDownloadUpperNum - 1),
        Accept: "application/json",
        Prefer: "count=exact",
      };
    }

    if (this.props.isLoggedIn && this.props.token) {
      preparedHeaders["Authorization"] = "Bearer " + this.props.token;
    }

    axios
      .get(url, { headers: preparedHeaders, requestId: "qbAxiosReq" })
      .then((response) => {
        this.setState(
          {
            dataFull: response.data,
            submitLoading: false,
            submitError: false,
            submitSuccess: true,
          },
          () => {
            this.timer = setTimeout(() => {
              this.setState({
                submitLoading: false,
                submitSuccess: false,
                submitError: false,
              });
            }, timeout);
            if (this.state.fileFormat === "delimited") {
              this.downloadTableWithDelimiter(true);
            } else if (this.state.fileFormat === "json") {
              this.downloadTableAsJSON(true);
            } else if (this.state.fileFormat === "xml") {
              this.downloadTableAsXML(true);
            } else if (this.state.fileFormat === "fasta") {
              this.downloadTableAsFASTA(true);
            }
          }
        );
      })
      .catch((error) => {
        console.error("HTTP Req:", error);
        this.setState(
          {
            dataFull: [],
            submitLoading: false,
            submitSuccess: true,
            submitError: true, // both true implies request successfully reported an error
          },
          () => {
            this.timer = setTimeout(() => {
              this.setState({
                submitLoading: false,
                submitSuccess: false,
                submitError: false,
              });
            }, timeout);
          }
        );
      });
  }

  render() {
    var style_container = {
      multiselectContainer: {
        // "height": "500px"
      },
      searchBox: {
        border: "none",
        "border-bottom": "1px solid grey",
        "border-radius": "0px",
        fontSize: "10px",
        height: "15px",
      },
      optionListContainer: {
        height: "500px !important",
      },
      inputField: {
        margin: "5px",
        fontSize: "10px",
        fontColor: "black",
      },
      optionContainer: {
        border: "1px solid grey",
      },
      option: {
        color: "black",
        fontSize: "15px",
        fontWeight: "400",
      },
      "option:hover": {
        backgroundColor: "rgb(155,125,64)",
        color: "white",
      },
      highlight: {
        color: "white",
        backgroundColor: "rgb(155,155,155)",
      },
      "highlight::hover": {
        backgroundColor: "rgb(155,125,64)",
        color: "white",
      },
      groupHeading: {
        color: "rgb(155,125,64)",
      },
    };
    return (
      <>
        <div className="paper_dredatawarehouse">
          <div className="paper_dredatawarehouse_noborder">
            <p className="title_dredatawarehouse">Download Query Results</p>

            <p className="title_dredatawarehouse2">File Format</p>
          </div>

          {/* FILE FORMAT RADIO GROUP */}

          <form>
            <div className="radiogroup_dredatawarehouse">
              <label className="radio_dredatawarehouse">
                <input
                  label="Delimited file (spreadsheet)"
                  value="delimited"
                  checked={this.state.fileFormat === "delimited"}
                  onChange={this.handleFileFormatChange}
                  type="radio"
                />
                <span className="radio_dredatawarehouse_checkmark"></span>
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  Delimited file (spreadsheet){" "}
                </span>
              </label>
              {this.state.fileFormat === "delimited" && (
                <div className="radio_dredatawarehouse_sub">
                  <p className="list_item_dredatawarehouse_text_secondary">
                    {"Use ; or \\t delimiter for sheet"}
                  </p>
                  <div className="wrap-input_dredatawarehouse">
                    <input
                      className="input_dredatawarehouse"
                      required
                      onChange={this.handleDelimiterChange}
                      label={"Use ; or \\t delimiter for sheet"}
                      value={this.state.delimiterChoice}
                      disabled={
                        this.state.fileFormat !== "delimited" ? true : false
                      }
                      id="delimiterInput"
                      type="text"
                      margin="none"
                    />
                    <span
                      className="focus-input_dredatawarehouse"
                      data-placeholder="&#xf207;"
                    ></span>
                  </div>
                </div>
              )}
              <label className="radio_dredatawarehouse">
                <input
                  label="JSON File"
                  value="json"
                  checked={this.state.fileFormat === "json"}
                  onChange={this.handleFileFormatChange}
                  type="radio"
                />
                <span className="radio_dredatawarehouse_checkmark"></span>
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  JSON File
                </span>
              </label>
              <label className="radio_dredatawarehouse">
                <input
                  label="XML File"
                  value="xml"
                  checked={this.state.fileFormat === "xml"}
                  onChange={this.handleFileFormatChange}
                  type="radio"
                />
                <span className="radio_dredatawarehouse_checkmark"></span>{" "}
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  XML File
                </span>
              </label>
              <label className="radio_dredatawarehouse">
                <input
                  label="FASTA File"
                  value="fasta"
                  checked={this.state.fileFormat === "fasta"}
                  // style={
                  //   this.identifySeqColumnInStateColumns() === null
                  //     ? styleSheet.hidden
                  //     : null
                  // }
                  onChange={this.handleFileFormatChange}
                  type="radio"
                />
                <span className="radio_dredatawarehouse_checkmark"></span>
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  FASTA File
                </span>
              </label>
              {this.state.fileFormat === "fasta" && (
                <div className="radio_dredatawarehouse_sub">
                  <p className="list_item_dredatawarehouse_text_secondary">
                    {"Note: FASTA header is composed from visible columns"}
                  </p>
                </div>
              )}
              <label className="radio_dredatawarehouse">
                <input
                  label="Copy single column values"
                  value="delimitedColumn"
                  checked={this.state.fileFormat === "delimitedColumn"}
                  onChange={this.handleFileFormatChange}
                  type="radio"
                />
                <span className="radio_dredatawarehouse_checkmark"></span>
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  Copy single column values{" "}
                </span>
              </label>
              {/* The options are loaded below in the <span>. This was needed because RadioGroup/FormControl does not work with a Span child element...*/}
            </div>
          </form>
          {this.state.fileFormat === "delimitedColumn" && (
            <div className="radio_dredatawarehouse_sub">
              <p className="list_item_dredatawarehouse_text_secondary">
                Choose a column
              </p>
              <Multiselect
                singleSelect={true}
                isObject={false}
                // selectedValues={this.state.columnChosen}
                options={this.state.columns}
                // showCheckbox={false}
                onSelect={this.handleMenuItemClick}
                avoidHighlightFirstOption={true}
                // closeIcon="cancel"
                placeholder={this.state.columns[this.state.columnChosen]}
                style={style_container}
              />
              <label className="radio_dredatawarehouse2">
                <input
                  label="Copy unique values only"
                  checked={this.state.copyUniqueValuesOnlyToggle}
                  onChange={this.handleCopyUniqueValuesOnlyToggle}
                  type="checkbox"
                />
                <span className="radio_dredatawarehouse_checkmark2"></span>
                <span className="radio_dredatawarehouse_checkmark_text">
                  {" "}
                  Copy unique values only{" "}
                </span>
              </label>
            </div>
          )}

          {/* ADDITIONAL DOWNLOADS OPTIONS */}
          <div className="paper_dredatawarehouse_noborder">
            <p className="title_dredatawarehouse">Options</p>
          </div>
          <form className="radio_dredatawarehouse">
            <label className="radio_dredatawarehouse2">
              <input
                label="Batch download"
                checked={this.state.batchDownloadCheckBox}
                disabled={
                  this.state.fileFormat === "delimitedColumn" ? true : false
                }
                onChange={this.handlebatchDownloadCheckBox}
                type="checkbox"
              />
              <span className="radio_dredatawarehouse_checkmark2"></span>
              <span className="radio_dredatawarehouse_checkmark_text">
                {" "}
                Batch download{" "}
              </span>
            </label>
            <span
              className={
                this.state.batchDownloadCheckBox !== true ||
                this.state.fileFormat === "delimitedColumn"
                  ? "hiden"
                  : "subtitle_dredatawarehouse"
              }
            >
              <div
                className={
                  isNaN(this.props.totalRows) === false &&
                  this.props.totalRows >= 0
                    ? "hiden"
                    : null
                }
              >
                <p className="subtitle_dredatawarehouse">
                  Re-run query with "Get exact row count" option selected
                </p>
              </div>
              <>
                <button
                  className={
                    this.state.batchSize === "10K"
                      ? "button_dredatawarehouse"
                      : "button_dredatawarehouse_altcolor"
                  }
                  onClick={(event) =>
                    this.handlebatchDownloadChange(event, "10K")
                  }
                  value={"10K"}
                >
                  {"10K"}
                </button>
                <button
                  className={
                    this.state.batchSize === "25K"
                      ? "button_dredatawarehouse"
                      : "button_dredatawarehouse_altcolor"
                  }
                  onClick={(event) =>
                    this.handlebatchDownloadChange(event, "25K")
                  }
                  value={"25K"}
                >
                  {"25K"}
                </button>
                <button
                  className={
                    this.state.batchSize === "100K"
                      ? "button_dredatawarehouse"
                      : "button_dredatawarehouse_altcolor"
                  }
                  onClick={(event) =>
                    this.handlebatchDownloadChange(event, "100K")
                  }
                  value={"100K"}
                >
                  {"100K"}
                </button>
                <button
                  className={
                    this.state.batchSize === "250K"
                      ? "button_dredatawarehouse"
                      : "button_dredatawarehouse_altcolor"
                  }
                  onClick={(event) =>
                    this.handlebatchDownloadChange(event, "250K")
                  }
                  value={"250K"}
                >
                  {"250K"}
                </button>
              </>
              <div>
                <p className="subtitle_dredatawarehouse">
                  {String(this.state.batchDownloadLowerNum).replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    ","
                  )}{" "}
                  to{" "}
                  {String(this.state.batchDownloadUpperNum).replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    ","
                  )}{" "}
                  of{" "}
                  {String(this.props.totalRows)
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    .replace("NaN", "unknown")}{" "}
                  rows
                </p>
              </div>
              <div>
                <button
                  className={"button_dredatawarehouse"}
                  onClick={(event) =>
                    this.handleLeftButtonClickRangeDownload(event)
                  }
                >
                  <FontAwesomeIcon icon={faChevronCircleLeft} size="2x" />
                </button>
                <button
                  className={"button_dredatawarehouse"}
                  onClick={(event) =>
                    this.handleRightButtonClickRangeDownload(event)
                  }
                >
                  <FontAwesomeIcon icon={faChevronCircleRight} size="2x" />
                </button>
              </div>
            </span>
            <label className="radio_dredatawarehouse2">
              <input
                label="Include table headers"
                checked={this.state.tableHeader}
                disabled={this.state.fileFormat !== "delimited" ? true : false}
                onChange={this.handleTableHeaderToggle}
                type="checkbox"
              />
              <span className="radio_dredatawarehouse_checkmark2"></span>
              <span className="radio_dredatawarehouse_checkmark_text">
                {" "}
                Include table headers{" "}
              </span>
            </label>
          </form>

          {/* FILE NAME INPUT */}
          <form
          // style={
          //   styleSheet.cardcardMarginLeftTop &&
          //   styleSheet.cardcardMarginBottomRight
          // }
          >
            <div className="radio_dredatawarehouse_sub">
              <div className="wrap-input_dredatawarehouse">
                <input
                  className="input_dredatawarehouse"
                  required
                  id="fileNameInput"
                  onChange={this.handleFileNameChange}
                  label={"File name"}
                  value={
                    this.state.fileNameCustom === ""
                      ? this.state.fileNameAuto
                      : this.state.fileNameCustom
                  }
                  disabled={this.state.fileFormat === "delimitedColumn"}
                  type="text"
                  margin="normal"
                />
              </div>
              <span
                className="focus-input_dredatawarehouse"
                data-placeholder="&#xf207;"
              ></span>
            </div>
          </form>

          {/* COPY FEATURE OUTPUT BOX + 2ND BUTTON */}
          {/*<div style={styleSheet.cardcardMarginLeftTop && styleSheet.cardcardMarginBottomRight}>
                    <TextField
                        id="copyOutput"
                        type="text"
                        label="Ctrl A and Ctrl C to copy"
                        value={this.state.copyResult}
                        onChange={this.handleCopyOutputClick}
                        style={this.state.copyResult === "" ? styleSheet.hidden : styleSheet.textFieldCopyOutput}
                        margin="normal" />
                    <IconButton onClick={this.insertToClipboard.bind(this, this.state.copyResult)} style={this.state.copyResult === "" ? styleSheet.hidden : styleSheet.button} aria-label="Copy">
                        <CopyIcon />
                    </IconButton>
                </div>*/}

          {this.state.copyLoading || this.state.submitLoading ? (
            <img
              src={require("../resources/progress.gif")}
              width="100%"
              alt="Progress indicator"
            />
          ) : (
            <hr />
          )}

          <button
            className="button_dredatawarehouse"
            onClick={this.handleDownloadClick}
            disabled={this.state.fileFormat === "delimitedColumn"}
            value={"Download"}
          >
            {"Download"}
          </button>
          <button
            className="button_dredatawarehouse"
            onClick={this.handleCopyClick}
            disabled={
              this.state.fileFormat !== "delimitedColumn" &&
              this.state.fileFormat !== "json" &&
              this.state.fileFormat !== "xml"
            }
            value={"Copy"}
          >
            {"Copy"}
          </button>
          <button
            className="button_dredatawarehouse_right_altcolor"
            onClick={this.handleResetClick}
            value={"Reset"}
          >
            {"Reset"}
          </button>
          {/* <Button style={styleSheet.button}>Help</Button> */}
        </div>

        <Snackbar
          open={this.state.snackBarVisibility}
          onClose={this.handleRequestClose}
          message={<span id="message-id">{this.state.snackBarMessage}</span>}
        />
      </>
    );
  }
}

const Snackbar = (props) => {
  if (props.open) {
    return (
      <div className="snackbar_dredatawarehouse">
        <p>{props.message}</p>
        <button onClick={props.onClose}>
          {" "}
          <FontAwesomeIcon icon={faTimes} size="2x" />{" "}
        </button>
      </div>
    );
  } else {
    return null;
  }
};
