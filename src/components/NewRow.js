import React from "react";
import Rodal from "rodal";
import "./styles.css";
import "./rodal.css";

import axios from "axios";

let lib = require("../utils/library");

export default class ResponsiveDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      table: props.table,
      columns: props.columns,
      allColumns: props.allColumns,
      primaryKeys: props.primaryKeys || [],
      qbFilters: props.qbFilters || [],
      url: props.url,
      inputVals: {},
      error: "",
      submitButtonLabel: "Submit",
    };
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      open: newProps.open,
      table: newProps.table,
      columns: newProps.columns,
      allColumns: newProps.allColumns,
      primaryKeys: newProps.primaryKeys || [],
      qbFilters: newProps.qbFilters || [],
      url: newProps.url,
      inputVals: {},
      error: "",
    });
  }

  handleClose = () => {
    this.props.handleNewRowClick(false);
  };

  // Reset all input fields
  handleReset = () => {
    clearTimeout(this.timer);
    let qbFiltersTemp = this.state.qbFilters;
    this.setState(
      {
        qbFilters: [],
        inputVals: {},
        error: "",
        submitButtonLabel: "Submit",
      },
      () => {
        this.setState({
          qbFilters: qbFiltersTemp,
        });
      }
    );
  };

  handleInput = (event, column, dataType) => {
    let value = event.target.value; // New value from user
    let inputValues = this.state.inputVals || {};

    if (value === "") {
      delete inputValues[column];
    } else {
      inputValues[column] = inputValues[column] || {};
      inputValues[column]["value"] = value;
      inputValues[column]["dataType"] = dataType;
    }

    this.setState({
      inputVals: inputValues,
    });
  };

  commitToChangeLog(newRow) {
    let primaryKey = {};
    for (let i = 0; i < this.state.primaryKeys.length; i++) {
      primaryKey[this.state.primaryKeys[i]] =
        newRow[0][this.state.primaryKeys[i]];
    }

    this.props.postReqToChangeLog(
      this.props.dbIndex,
      new Date(Date.now()).toISOString(),
      this.state.table,
      primaryKey,
      "ROW_INSERT",
      "{}",
      newRow[0],
      "ROW INSERTED.",
      "public"
    );
  }

  handleSubmit = () => {
    if (this.state.submitButtonLabel === "Submit") {
      // Give user 4 seconds to confirm
      this.setState({
        submitButtonLabel: "Are you sure?",
      });
      this.timer = setTimeout(() => {
        this.setState({
          submitButtonLabel: "Submit",
        });
      }, 4000);
    } else {
      // User wants to submit for sure, so do it
      clearTimeout(this.timer);
      this.setState({
        submitButtonLabel: "Submit",
      });
      let input = this.state.inputVals;
      let keys = Object.keys(this.state.inputVals);

      let newRowURL =
        lib.getDbConfig(this.props.dbIndex, "url") + "/" + this.state.table;
      let postReqBody = {};

      for (let i = 0; i < keys.length; i++) {
        let column = keys[i];
        let rawValue = input[keys[i]]["value"];
        let dataType = input[keys[i]]["dataType"];
        let value = rawValue;

        if (dataType === "string") {
          value = String(rawValue);
        } else if (dataType === "integer" || dataType === "double") {
          value = Number(rawValue);
        } else if (dataType === "boolean") {
          value = Boolean(rawValue);
        }

        postReqBody[column] = value;
      }

      let preparedHeaders = { Prefer: "return=representation" };
      if (this.props.isLoggedIn && this.props.token) {
        preparedHeaders["Authorization"] = "Bearer " + this.props.token;
      }

      axios
        .post(newRowURL, postReqBody, { headers: preparedHeaders })
        .then((response) => {
          this.commitToChangeLog(response.data);
          this.props.insertNewRow(response.data);
          this.handleReset();
          this.props.handleNewRowClick(false);
        })
        .catch((error) => {
          this.setState(
            {
              error: error.response,
            },
            () => {
              let element = document.getElementById("errorPaper");
              element.scrollIntoView();
            }
          );
        });
    }
  };

  render() {
    let tableRename = lib.getTableConfig(
      this.props.dbIndex,
      this.state.table,
      "rename"
    );
    let tableDisplayName = tableRename ? tableRename : this.state.table;

    return (
      <>
        <Rodal
          visible={this.state.open}
          onClose={this.handleClose}
          width={700}
          customStyles={{
            height: "auto",
            bottom: "auto",
            top: "auto",
          }}
          closeOnEsc={true}
        >
          <div className="paper_dredatawarehouse_noborder">
            <p className="title_dredatawarehouse">
              {"Insert new row to " + tableDisplayName}
            </p>
          </div>
          <div>
            {this.state.error !== "" && (
              <div
                className="paper_dredatawarehouse_error"
                id="errorPaper"
                elevation={4}
              >
                <p className="title_dredatawarehouse_error">Request Denied</p>
                <p className="subtitle_dredatawarehouse_error">
                  {"Code: " +
                    (this.state.error && this.state.error.data
                      ? this.state.error.data.code
                      : "")}
                </p>
                <p className="subtitle_dredatawarehouse_error">
                  {"Message: " +
                    (this.state.error && this.state.error.data
                      ? this.state.error.data.message
                      : "")}
                </p>
                <p className="subtitle_dredatawarehouse_error">
                  {"Details: " +
                    (this.state.error && this.state.error.data
                      ? this.state.error.data.details
                      : "")}
                </p>
              </div>
            )}
            <div className="paper_dredatawarehouse_noborder">
              <p className="list_item_dredatawarehouse_text_secondary">
                {"Unique values for the table's primary key (" +
                  this.state.primaryKeys.join(", ") +
                  ") are mandatory (*). Other constraints may be imposed by the database schema, follow instructions in the error details."}
              </p>
            </div>
            <div className="paper_dredatawarehouse_noborder">
              <p className="subtitle_dredatawarehouse">{"New Row"}</p>
              <>
                {this.state.qbFilters.map((column) => {
                  return (
                    <div
                      key={column.id}
                      className={"radio_dredatawarehouse_sub_nomargin"}
                    >
                      <p className="list_item_dredatawarehouse_text_secondary">
                        {column.label ? column.label : column.id}
                      </p>
                      <div className="wrap-input_dredatawarehouse">
                        <input
                          className="input_dredatawarehouse"
                          required={
                            this.state.primaryKeys.indexOf(column.id) >= 0
                          }
                          onChange={(e) =>
                            this.handleInput(e, column.id, column.type)
                          }
                          key={column.id}
                          label={column.label ? column.label : column.id}
                          placeholder={column.type}
                          value={
                            column.default_value ||
                            (this.state.inputVals[column.id]
                              ? this.state.inputVals[column.id]["value"]
                              : "") ||
                            ""
                          }
                          type="text"
                          margin="normal"
                        />
                        <span
                          className="focus-input_dredatawarehouse"
                          data-placeholder="&#xf207;"
                        ></span>
                      </div>
                    </div>
                  );
                })}
              </>
            </div>
          </div>
          <hr />

          <div>
            <button
              className="button_dredatawarehouse_right"
              onClick={this.handleSubmit}
              value={this.state.submitButtonLabel}
              autoFocus
            >
              {this.state.submitButtonLabel}
            </button>
            <button
              className="button_dredatawarehouse_right_altcolor"
              onClick={this.handleReset}
              value={"Reset"}
            >
              {"Reset"}
            </button>
            <button
              className="button_dredatawarehouse_right_altcolor"
              onClick={this.handleClose}
              value={"Cancel"}
            >
              {"Cancel"}
            </button>
          </div>
        </Rodal>
      </>
    );
  }
}
