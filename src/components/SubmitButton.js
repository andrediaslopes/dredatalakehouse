import React from "react";
import "./styles.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleRight,
  faCheckCircle,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";

export const SubmitButton = (props) => {
  let buttonClass = "submitbutton_dredatawarehouse_standard";
  if (props.success) {
    buttonClass = "submitbutton_dredatawarehouse_sucess";
  }
  if (props.success && props.error) {
    buttonClass = "submitbutton_dredatawarehouse_error";
  }

  return (
    <div className="submitbutton_dredatawarehouse">
      <div className={buttonClass} onClick={props.getRules}>
        {props.success ? (
          props.error ? (
            <FontAwesomeIcon icon={faTimesCircle} size="4x" />
          ) : (
            <FontAwesomeIcon icon={faCheckCircle} size="4x" />
          )
        ) : (
          <FontAwesomeIcon icon={faArrowAltCircleRight} size="4x" />
        )}
      </div>
      {props.loading && <CircularProgress />}
    </div>
  );
};

function CircularProgress() {
  return (
    <div className={"circular_progress_dredatawarehouse"}>
      <div className={"loader-component-lazy-center"}>
        <svg version="1.1" id="L7" x="0px" y="0px" viewBox="0 0 100 100">
          <path
            fill="#00743f"
            d="M31.6,3.5C5.9,13.6-6.6,42.7,3.5,68.4c10.1,25.7,39.2,38.3,64.9,28.1l-3.1-7.9c-21.3,8.4-45.4-2-53.8-23.3
              c-8.4-21.3,2-45.4,23.3-53.8L31.6,3.5z"
          >
            <animateTransform
              attributeName="transform"
              attributeType="XML"
              type="rotate"
              dur="2s"
              from="0 50 50"
              to="360 50 50"
              repeatCount="indefinite"
            />
          </path>
        </svg>
      </div>
    </div>
  );
}
