import React, { Component } from "react";
import {withStyles} from '@material-ui/core/styles'

import Help from "./Help.js";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";

import SearchIcon from "@material-ui/icons/Search";
import MenuIcon from "@material-ui/icons/Menu";
import HistoryIcon from "@material-ui/icons/History";
import HelpIcon from "@material-ui/icons/HelpOutline";

import Button from "@material-ui/core/Button";

let _ = require("lodash");
let lib = require("../utils/library");

//join: predicted genes, protein seqs
export default class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearchBarFdpOpen: false,
      isLoginFdpOpen: null,
      loginDialogOpen: false,
      isHelpOpen: false,
    };
    this.changeSearchTermDebounce = _.debounce((value) => {
      this.props.changeSearchTerm(value);
      this.setState({
        isSearchBarFdpOpen: true,
      });
    }, 350);
  }

  changeSearchTerm(e) {
    /*if (e && ((e.key && e.key === 'Enter') || !e.target.value)) {
			this.props.changeSearchTerm(e.target.value);
		}*/
    this.changeSearchTermDebounce(e.target.value);
  }

  render() {
    let dbTitle =
      lib.getDbConfig(this.props.dbIndex, "title") || "Untitled database";
    let searchBarFdpOpenStyles = null;
    if (this.state.isSearchBarFdpOpen) {
      searchBarFdpOpenStyles = {
        backgroundColor: "white",
        border: "1px solid grey",
        width: 325 + "px",
        minWidth: "inherit",
      };
    } else {
      searchBarFdpOpenStyles = {
        backgroundColor: "rgba(255, 255, 255, 0.1)",
        border: "none",
        width: 45 + "%",
        maxWidth: 525 + "px",
        minWidth: 325 + "px",
      };
    }

    // Set a short window title
    document.title = dbTitle
      .replace("Database", "db")
      .replace("database", "db");

    return (
      <>
        <AppBar position="absolute">
          <Toolbar>
            <Typography
              variant="h6"
              color="inherit"
              // style={styleSheet.dbTitleFlex}
            >
              {dbTitle}
            </Typography>

            <div style={styleSheet.searchBarFlex}>
              <TextField
                id="search"
                placeholder="Search"
                onKeyPress={this.changeSearchTerm.bind(this)}
                onChange={this.changeSearchTerm.bind(this)}
                onFocus={this.changeSearchTerm.bind(this)}
                type="search"
                style={{ ...styleSheet.searchBar, ...searchBarFdpOpenStyles }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon
                        style={
                          this.state.isSearchBarFdpOpen
                            ? { fill: "rgba(0,0,0,0.5)" }
                            : { fill: "rgba(255,255,255,0.75)" }
                        }
                      />
                    </InputAdornment>
                  ),
                }}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="off"
                spellCheck="false"
              />
            </div>
          </Toolbar>
        </AppBar>
      </>
    );
  }
}

// const styleSheet = {
//   dbTitleFlex: {
//     flex: 0.3,
//   },
//   searchBarFlex: {
//     flex: 0.6,
//     display: "block",
//     marginLeft: 5,
//     marginRight: 5,
//     marginTop: 0,
//   },
//   searchBar: {
//     marginLeft: 5,
//     marginRight: 5,
//     background: "white",
//     padding: 10,
//     paddingBottom: 5,
//     borderRadius: 3,
//     float: "right",
//     transition: "all 0.2s",
//   },
//   rightIconsFlex: {
//     flex: 0.05,
//     display: "block",
//   },
//   floatRight: {
//     float: "right",
//   },
//   floatRightPadded: {
//     float: "right",
//     marginRight: 5,
//   },
//   button: {
//     margin: 15,
//   },
// };
