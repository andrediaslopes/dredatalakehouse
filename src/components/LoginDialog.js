import React, { Component } from "react";
import Rodal from "rodal";
import "./styles.css";
import "./rodal.css";

export default class LoginDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleSingleSignOn = this.handleSingleSignOn.bind(this);
  }

  handleLoginClick(e) {
    if (this.state.email && this.state.password) {
      this.props.setUserEmailPassword(this.state.email, this.state.password);
      this.props.handleLoginDialogCloseClick(e);
    }
  }

  handleSingleSignOn(e) {
    this.props.SingleSignOn();
    this.props.handleLoginDialogCloseClick(e);
  }

  onChangeHandler(e) {
    this.setState({
      [e.target.id]: e.target.value,
    });
  }

  render() {
    if (this.props.open || false) {
      return (
        <Rodal
          visible={this.props.open}
          onClose={this.props.handleLoginDialogCloseClick}
          width={700}
          customStyles={{
            height: "auto",
            bottom: "auto",
            top: "auto",
          }}
          closeOnEsc={true}
        >
          <form className="paper_dredatawarehouse_noborder">
            <p id="form-dialog-title" className="title_dredatawarehouse">
              Login{this.props.dbName ? " - " + this.props.dbName : ""}
            </p>
            <div>
              <p className="list_item_dredatawarehouse_text_secondary">
                Provide your credentials for this database, it may allow you
                more privileges. You can also do single sign on to enter with
                your user acess.
              </p>
              <div className={"paper_dredatawarehouse_noborder"}>
                <div className={"radio_dredatawarehouse_sub_nomargin"}>
                  <div className="wrap-input_dredatawarehouse">
                    <input
                      className="input_dredatawarehouse"
                      autoFocus
                      margin="dense"
                      id="email"
                      label="Email Address"
                      type="email"
                      placeholder="Email"
                      onChange={this.onChangeHandler}
                      fullWidth
                    />
                    <span
                      className="focus-input_dredatawarehouse"
                      data-placeholder="&#xf207;"
                    ></span>
                  </div>
                </div>
                <div className={"radio_dredatawarehouse_sub_nomargin"}>
                  <div className="wrap-input_dredatawarehouse">
                    <input
                      className="input_dredatawarehouse"
                      margin="dense"
                      id="password"
                      label="Password"
                      type="password"
                      placeholder="Password"
                      onChange={this.onChangeHandler}
                      fullWidth
                    />
                    <span
                      className="focus-input_dredatawarehouse"
                      data-placeholder="&#xf207;"
                    ></span>
                  </div>
                </div>
              </div>
            </div>
            {/* <Divider /> */}
            <div>
              <button
                className="button_dredatawarehouse_right"
                onClick={this.handleLoginClick}
                value={"Login"}
              >
                Login
              </button>
              <button
                className="button_dredatawarehouse_right_altcolor"
                onClick={this.handleLoginDialogCloseClick}
                value={"Cancel"}
              >
                {"Cancel"}
              </button>
              <button
                className="button_dredatawarehouse_right"
                onClick={this.handleSingleSignOn}
                value={"Single SignOn"}
                autoFocus
              >
                Single SignOn
              </button>
            </div>
          </form>
        </Rodal>
      );
    } else return null;
  }
}
