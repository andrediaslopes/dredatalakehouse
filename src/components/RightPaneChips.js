import React, { Component } from "react";
export default class RightPaneChips extends Component {
  constructor(props) {
    super(props);

    // rows = number of rows in response
    // totalRows = number of rows in query result (i.e. full result count)
    // rowLimit = user set row limit
    // maxRows = app wide limit on max value for rowLimit
    this.state = {
      rows: props.rows ? props.rows : 0,
      totalRows: props.totalRows ? props.totalRows : 0,
      rowLimit: props.rowLimit ? props.rowLimit : 2500,
      maxRows: props.maxRows ? props.maxRows : 250000,
      tip: "Tip: Hold shift and click to multi-sort!",
      tip2: "Increase row-limit for full result.",
      title2: "",
      tip3: "Use Batch Download option.",
      isTip1FdpOpen: false,
      isTip2FdpOpen: false,
      isTip3FdpOpen: false,
    };
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      rows: newProps.rows ? newProps.rows : 0,
      totalRows: newProps.totalRows ? newProps.totalRows : 0,
      rowLimit: newProps.rowLimit ? newProps.rowLimit : 2500,
      maxRows: newProps.maxRows ? newProps.maxRows : 100000,
    });
  }
  render() {
    let rowCountChipLabel =
      "Displaying " +
      String(this.props.rows || 0).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
      " rows";
    if (this.props.totalRows >= 0) {
      rowCountChipLabel =
        "Displaying " +
        String(this.props.rows || 0).replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
        " of " +
        String(this.props.totalRows || 0).replace(
          /\B(?=(\d{3})+(?!\d))/g,
          ","
        ) +
        " rows";
    }
    return (
      <div>
        <Chip
          color={"primary"}
          variant="outlined"
          label={rowCountChipLabel}
          key={1}
        />

        {this.state.rows === this.state.rowLimit &&
        this.state.rows !== this.state.maxRows ? (
          <Chip
            color={"secondary"}
            label={this.state.tip2}
            key={3}
            onClick={() => {
              this.props.increaseRowLimit(this.state.maxRows);
            }}
          />
        ) : (
          <div />
        )}
        {this.state.rows === this.state.maxRows ? (
          <Chip
            label={this.state.tip3}
            key={2}
            color="secondary"
            variant="secondary"
          />
        ) : (
          <div />
        )}
        <Chip color="secondary" label={this.state.tip} key={4} />
      </div>
    );
  }
}

const Chip = (props) => {
  if (props.color === "secondary") {
    return (
      <div className="Chip_dredatawarehouse_alt">
        <p className="Chip_dredatawarehouse_label_alt"> {props.label}</p>
        <p className="Chip_dredatawarehouse_title_alt"> {props.title} </p>
      </div>
    );
  }
  return (
    <div className="Chip_dredatawarehouse">
      <p className="Chip_dredatawarehouse_label"> {props.label}</p>
      <p className="Chip_dredatawarehouse_title"> {props.title} </p>
    </div>
  );
};
