import React from "react";
import "./styles.css";

import DbPicker from "./DbPicker";
import DbSchema from "./DbSchema.js";

export const LeftPane = (props) => {
  let classNames =
    props.leftPaneVisibility === true
      ? "left_pane_dredatawarehouse"
      : "left_pane_dredatawarehouse left_pane_noleftpane_dredatawarehouse hiden";
  return (
    <div className={classNames}>
      <DbPicker {...props} />
      <DbSchema {...props} />
    </div>
  );
};
