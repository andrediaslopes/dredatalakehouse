import React, { Component } from "react";
import "./styles.css";
import NewRow from "./NewRow.js";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faEdit,
  faTrash,
  faExclamationTriangle,
  faTimesCircle,
  faEraser,
  faToggleOff,
  faToggleOn,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";

//const timeout = 2000;

export default class EditCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table: props.table,
      columns: props.columns,
      url: props.url,

      dbPkInfo: {},
      primaryKeysAvailable: false,
      primaryKeys: [],

      featureEnabled: props.featureEnabled | false,
      changesMade: props.changesMade | {},
      rowsStrikedOut: props.rowsStrikedOut,

      snackBarVisibility: false,
      snackBarMessage: "Unknown error occured",

      submitButtonLabel: "Submit",
      removeButtonLabel: "Remove All",

      newRowDialogOpen: false,
    };

    this.handleFeatureEnabledSwitch = this.handleFeatureEnabledSwitch.bind(
      this
    );
    this.handleSubmitClick = this.handleSubmitClick.bind(this);
    this.handleNewRowClick = this.handleNewRowClick.bind(this);
    this.handleRemoveAllClick = this.handleRemoveAllClick.bind(this);
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      table: newProps.table,
      columns: newProps.columns,
      url: newProps.url,
      dbPkInfo: newProps.dbPkInfo,
      primaryKeys: [],
      primaryKeysAvailable: false,
      changesMade: newProps.changesMade,
      rowsStrikedOut: newProps.rowsStrikedOut,
      featureEnabled: newProps.featureEnabled,
    });

    // Enable PK related features if table has a PK
    if (newProps.dbPkInfo && this.state.table) {
      for (let i = 0; i < newProps.dbPkInfo.length; i++) {
        if (newProps.dbPkInfo[i]["table"] === this.state.table) {
          this.setState({
            primaryKeys: newProps.dbPkInfo[i]["primary_keys"],
            primaryKeysAvailable:
              JSON.stringify(newProps.dbPkInfo[i]["primary_keys"]) !== "[]",
          });
        }
      }
    }
  }

  handleRemoveAllClick(e) {
    if (this.state.removeButtonLabel === "Remove All") {
      this.setState({
        removeButtonLabel: "Are You Sure?",
      });
      this.timer = setTimeout(() => {
        this.setState({
          removeButtonLabel: "Remove All",
        });
      }, 4000);
    } else {
      clearTimeout(this.timer);
      this.setState({
        removeButtonLabel: "Remove All",
      });
      this.props.deleteTableChanges();
    }
  }

  // Opens a dialog to allow user to insert a new row to the table
  handleNewRowClick(newState) {
    if (newState === false || newState === true) {
      this.setState({
        newRowDialogOpen: newState,
      });
    } else {
      this.setState({
        newRowDialogOpen: !this.state.newRowDialogOpen,
      });
    }
  }

  handleSubmitClick() {
    console.clear();

    if (this.state.submitButtonLabel === "Submit") {
      this.setState({
        submitButtonLabel: "Are you sure?",
      });
      this.timer = setTimeout(() => {
        this.setState({
          submitButtonLabel: "Submit",
        });
      }, 4000);
    } else {
      clearTimeout(this.timer);
      this.setState({
        submitButtonLabel: "Submit",
      });
      this.props.submitChanges();
    }
  }

  // Toggle the switch
  handleFeatureEnabledSwitch() {
    // Set the featureEnabled state to opposite of what it is at the moment...
    this.setState(
      {
        featureEnabled: !this.state.featureEnabled,
      },
      () => {
        this.props.changeEditFeatureEnabled(this.state.featureEnabled);
      }
    );
  }

  // Converts the JSON object for PK into a string that can be displayed to user
  primaryKeyStringify(primaryKey) {
    let keys = Object.keys(primaryKey);
    let stringified = "";

    for (let i in Object.keys(primaryKey)) {
      stringified += keys[i] + " = " + primaryKey[keys[i]];
      if (parseInt(i, 10) !== keys.length - 1) {
        stringified += " and ";
      }
    }
    return stringified;
  }

  // Creates the <list> that shows the changes made
  createChangeLogList() {
    if (
      this.state.table === "" ||
      JSON.stringify(this.state.changesMade) === "{}" ||
      this.state.changesMade[this.state.table] === null ||
      this.state.changesMade[this.state.table] === undefined
    ) {
      return (
        <div className="list_item_dredatawarehouse" key={-1}>
          <div className="list_item_dredatawarehouse_icon" key={-1}>
            <FontAwesomeIcon icon={faSearch} size="2x" />
          </div>
          <p className="list_item_dredatawarehouse_text">
            {" "}
            No unsubmitted changes found...{" "}
          </p>
        </div>
      );
    }

    let length = Object.keys(this.state.changesMade[this.state.table]).length;
    let keys = Object.keys(this.state.changesMade[this.state.table]);
    let listItems = [];

    for (let i = 0; i < length; i++) {
      let column = keys[i];

      let change = this.state.changesMade[this.state.table][column];
      let changeCount = Object.keys(change).length;

      for (let ii = 0; ii < changeCount; ii++) {
        let oldValue = change[Object.keys(change)[ii]]["oldValue"];
        let newValue = change[Object.keys(change)[ii]]["newValue"];
        let markForDeletion = change[Object.keys(change)[ii]]["delete"];
        let primaryKey = change[Object.keys(change)[ii]]["primaryKey"];
        let error = change[Object.keys(change)[ii]]["error"];
        let errorResp = change[Object.keys(change)[ii]]["errorResp"];

        listItems.push(
          <span key={String(i) + String(ii) + "span"}>
            <div
              className="list_item_dredatawarehouse"
              key={String(i) + String(ii)}
              title={error ? errorResp : ""}
            >
              <div className="list_item_dredatawarehouse_icon">
                {error ? (
                  <FontAwesomeIcon icon={faTimesCircle} size="2x" />
                ) : markForDeletion ? (
                  <FontAwesomeIcon icon={faEraser} size="2x" />
                ) : (
                  <FontAwesomeIcon icon={faEdit} size="2x" />
                )}
              </div>
              {markForDeletion ? (
                <ListItemText
                  primary={"Delete row"}
                  secondary={"Where " + this.primaryKeyStringify(primaryKey)}
                />
              ) : (
                <ListItemText
                  primary={column + " column changed"}
                  secondary={
                    "From '" +
                    oldValue +
                    "' to '" +
                    newValue +
                    "' where " +
                    this.primaryKeyStringify(primaryKey)
                  }
                />
              )}

              <button
                className="list_item_dredatawarehouse_icon_button"
                onClick={this.props.deleteChange.bind(
                  this,
                  column,
                  Object.keys(change)[ii],
                  markForDeletion || false
                )}
              >
                <FontAwesomeIcon icon={faTrash} size="2x" />
              </button>
            </div>

            {error && errorResp && errorResp.code && errorResp.message && (
              <div key={String(i) + String(ii) + "err"}>
                <ListItemText
                  primary={"PostgreSQL Error Code: " + errorResp.code}
                  secondary={"Error: " + errorResp.message}
                />
              </div>
            )}
          </span>
        );
      }
    }
    return listItems;
  }

  render() {
    return (
      <>
        <div className="paper_dredatawarehouse">
          <div className="paper_dredatawarehouse_noborder">
            <p className="title_dredatawarehouse">Edit Table Contents</p>
          </div>

          {this.state.primaryKeysAvailable ? (
            <div className="list_item_dredatawarehouse">
              <button
                className={
                  this.state.featureEnabled
                    ? "list_item_dredatawarehouse_icon_switch_on_icon"
                    : "list_item_dredatawarehouse_icon_switch_off_icon"
                }
                checked={this.state.featureEnabled}
                onClick={this.handleFeatureEnabledSwitch}
              >
                {this.state.featureEnabled ? (
                  <FontAwesomeIcon icon={faToggleOn} size="2x" />
                ) : (
                  <FontAwesomeIcon icon={faToggleOff} size="2x" />
                )}
              </button>
              <p className="list_item_dredatawarehouse_text">
                {" "}
                Enable table edit features{" "}
              </p>
            </div>
          ) : (
            <>
              <div className="list_item_dredatawarehouse" key={-1}>
                <div className="list_item_dredatawarehouse_icon" key={-1}>
                  <FontAwesomeIcon icon={faExclamationTriangle} size="2x" />
                </div>
                <p className="list_item_dredatawarehouse_text">
                  {" "}
                  This table cannot be edited because its primary keys were not
                  found.{" "}
                </p>
              </div>
            </>
          )}

          {this.state.featureEnabled && this.state.primaryKeysAvailable ? (
            <>
              <div className="paper_dredatawarehouse_noborder">
                <p className="subtitle_dredatawarehouse">
                  Changes made to this table
                </p>
              </div>

              <div>{this.createChangeLogList()}</div>
            </>
          ) : JSON.stringify(this.state.changesMade) !== "{}" ? (
            <>
              <div className="list_item_dredatawarehouse" key={-1}>
                <div className="list_item_dredatawarehouse_icon" key={-1}>
                  <FontAwesomeIcon icon={faExclamationTriangle} size="2x" />
                </div>
                <p className="list_item_dredatawarehouse_text">
                  {" "}
                  Unsubmitted changes are detected, these changes will be lost
                  if not submitted.{" "}
                </p>
              </div>
            </>
          ) : (
            <div />
          )}
          <hr />

          <button
            className="button_dredatawarehouse"
            onClick={this.handleSubmitClick}
            disabled={
              !(this.state.featureEnabled && this.state.primaryKeysAvailable)
            }
            value={this.state.submitButtonLabel}
          >
            {this.state.submitButtonLabel}
          </button>
          <button
            className="button_dredatawarehouse"
            onClick={() => this.handleNewRowClick(true)}
            disabled={
              !(this.state.featureEnabled && this.state.primaryKeysAvailable)
            }
            value={"New Row"}
          >
            {"New Row"}
          </button>
          <button
            className="button_dredatawarehouse_right_altcolor"
            onClick={this.handleRemoveAllClick}
            disabled={
              !(this.state.featureEnabled && this.state.primaryKeysAvailable)
            }
            value={this.state.removeButtonLabel}
          >
            {this.state.removeButtonLabel}
          </button>
        </div>

        <NewRow
          {...this.props}
          open={this.state.newRowDialogOpen}
          primaryKeys={this.state.primaryKeys}
          handleNewRowClick={this.handleNewRowClick}
        />

        <Snackbar
          open={this.state.snackBarVisibility}
          onClose={this.handleRequestClose}
          message={<span id="message-id">{this.state.snackBarMessage}</span>}
        />
      </>
    );
  }
}

const ListItemText = (props) => {
  return (
    <div className="list_item_dredatawarehouse_list">
      <p className="list_item_dredatawarehouse_text_primary">{props.primary}</p>
      <p className="list_item_dredatawarehouse_text_secondary">
        {props.secondary}
      </p>
    </div>
  );
};

const Snackbar = (props) => {
  if (props.open) {
    return (
      <div className="snackbar_dredatawarehouse">
        <p>{props.message}</p>
        <button onClick={props.onClose}>
          {" "}
          <FontAwesomeIcon icon={faTimes} size="2x" />{" "}
        </button>
      </div>
    );
  } else {
    return null;
  }
};
