import React, { Component } from "react";
import "./styles.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faShareAlt,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";

let _ = require("lodash");
let lib = require("../utils/library");
let displayLengthCutoff = 50;

export default class HistoryPane extends Component {
  // changeDisplayIndexDebounce;
  // timer;

  constructor(props) {
    super(props);

    let localHistoryArray = JSON.parse(
      localStorage.getItem("localHistory") || "[]"
    );
    localHistoryArray =
      JSON.stringify(localHistoryArray) === "[]" ? null : localHistoryArray;

    // historyArray will have the latest URL at the end ... i.e. 0 position is the earliest query, and the highest position index is the latest query...
    // TODO: Need to make historyArray db specific!!!
    this.state = {
      newHistoryItem: this.props.newHistoryItem,
      displayIndex: -1,
      historyArray: localHistoryArray ? localHistoryArray : [],
      deleteHistoryDialogVisibilityStyles: "hiden",
      snackBarVisibility: false,
      snackBarMessage: "Unknown error occured",
    };
    this.changeDisplayIndexDebounce = _.debounce(
      (value) => this.setState({ displayIndex: value }),
      300
    );

    this.closeDrawer = this.closeDrawer.bind(this);
    this.showDeleteHistoryDialog = this.showDeleteHistoryDialog.bind(this);
    this.deleteHistory = this.deleteHistory.bind(this);
  }

  // Keeps track of the incoming queries in an array
  componentWillReceiveProps(newProps) {
    // If the incoming newHistoryItem isn't already the current state.newHistoryItem AND it actually exists THEN
    if (
      this.state.newHistoryItem !== newProps.newHistoryItem &&
      newProps.newHistoryItem !== [] &&
      newProps.newHistoryItem !== undefined &&
      newProps.newHistoryItem !== null &&
      newProps.newHistoryItem
    ) {
      // Check if the new item already exists in the historyArray
      if (
        lib.inArray(newProps.newHistoryItem, this.state.historyArray) === false
      ) {
        // doesn't exist, so insert it at highestIndex+1 position (i.e. 0th index is oldest)
        var arrayvar = this.state.historyArray.slice();
        arrayvar.push(newProps.newHistoryItem);

        this.setState(
          {
            newHistoryItem: newProps.newHistoryItem,
            historyArray: arrayvar,
          },
          () => {
            try {
              localStorage.setItem(
                "localHistory",
                JSON.stringify(this.state.historyArray)
              );
            } catch (e) {
              console.error(e);
            }
          }
        );
      } else {
        // already exists, move it to "top" (which in this case is the highest index...)
        this.setState(
          {
            newHistoryItem: newProps.newHistoryItem,
            historyArray: lib.moveArrayElementFromTo(
              this.state.historyArray,
              lib.elementPositionInArray(
                newProps.newHistoryItem,
                this.state.historyArray
              ),
              this.state.historyArray.length - 1
            ),
          },
          () => {
            try {
              localStorage.setItem(
                "localHistory",
                JSON.stringify(this.state.historyArray)
              );
            } catch (e) {
              console.error(e);
            }
          }
        );
      }
    }
  }

  // Loads the History Item in the Query Builder
  handleHistoryItemClick(index) {
    let url = this.state.historyArray[index][0];
    let rules = this.state.historyArray[index][1];

    this.props.changeTable(this.extractTableNameFromURL(url, true));
    this.props.changeRules(rules);
  }

  // Inserts shareable URL to clipboard
  handleLinkIconClick(index) {
    let error = false,
      insertSuccess = false;

    let url = this.state.historyArray[index][0];
    let rules = this.state.historyArray[index][1];

    // Extract the table name from URL
    let tableRx = /\/\w+/g;
    let tableName;
    let tableNameRegExp = tableRx.exec(
      url.replace(lib.getDbConfig(this.props.dbIndex, "url"), "")
    );
    if (tableNameRegExp) {
      tableName = tableNameRegExp[0].replace(/\//g, "");
    } else {
      tableName = null;
      error = true;
    }

    // Create the URL needed for sharing
    let shareUrl = "";
    if (!error) {
      shareUrl =
        window.location.origin + "/front-ends/dredatawarehouse" +
        "/queryBuilder/db/" +
        this.props.dbIndex +
        "/table/" +
        tableName;
      if (rules !== null) {
        shareUrl += "?query=" + encodeURIComponent(JSON.stringify(rules));
      }

      // Insert to clipboard
      insertSuccess = this.insertToClipboard(shareUrl);
    }

    // if no errors, show a successfully inserted message to user...
    if (!error && insertSuccess) {
      this.setState(
        {
          snackBarVisibility: true,
          snackBarMessage: "Link copied!",
        },
        () => {
          this.timer = setTimeout(() => {
            this.setState({
              snackBarVisibility: false,
              snackBarMessage: "Unknown error",
            });
          }, 2500);
        }
      );
    }
  }

  insertToClipboard(str) {
    //based on https://stackoverflow.com/a/12693636
    document.oncopy = function (event) {
      if (event && event.clipboardData) {
        event.clipboardData.setData("Text", str);
        event.preventDefault();
      }
    };
    let copySuccess = document.execCommand("Copy");
    document.oncopy = null;
    return copySuccess;
  }

  closeDrawer() {
    this.props.closeHistoryPane();
  }

  extractTableNameFromURL(url, getRaw = false) {
    let rawTableName = url
      .replace(lib.getDbConfig(this.props.dbIndex, "url"), "")
      .replace(/\?.*/, "")
      .replace(/\s/g, "")
      .replace("/", "");

    if (getRaw) {
      return rawTableName;
    }

    let tableRename = lib.getTableConfig(
      this.props.dbIndex,
      rawTableName,
      "rename"
    );
    let displayName = tableRename ? tableRename : rawTableName;

    return displayName;
  }

  cleanUpRules(url) {
    return url
      .replace(lib.getDbConfig(this.props.dbIndex, "url"), "")
      .replace(/.*\?/, "")
      .replace(/&/g, "\n")
      .replace(/,/g, ",\n")
      .replace(/limit=\d*/g, "");
  }

  recursiveRulesExtraction(rules, condition, depth = 0) {
    let rulesArray = [];
    if (rules.length > 1) {
      rulesArray = [[Array(depth).join("\t") + condition]];
    }
    for (let i = 0; i < rules.length; i++) {
      let potentialName = rules[i]["field"];
      if (potentialName !== null && potentialName !== undefined) {
        rulesArray.push([
          Array(depth + 1).join("\t") + potentialName,
          rules[i]["operator"],
          rules[i]["value"],
        ]);
      } else {
        // Check if it's a GROUP by looking for "condition" key
        if (rules[i]["condition"] === "AND" || rules[i]["condition"] === "OR") {
          let subGroupRules = this.recursiveRulesExtraction(
            rules[i]["rules"],
            rules[i]["condition"],
            depth + 1
          );
          for (let ii = 0; ii < subGroupRules.length; ii++) {
            if (subGroupRules[ii] !== null && subGroupRules[ii] !== undefined) {
              rulesArray.push(subGroupRules[ii]);
            }
          }
        }
      }
    }
    return rulesArray;
  }

  changeDisplayIndex(newDisplayIndex) {
    this.changeDisplayIndexDebounce(newDisplayIndex);
  }

  showDeleteHistoryDialog() {
    if (this.state.deleteHistoryDialogVisibilityStyles === null) {
      this.setState({
        deleteHistoryDialogVisibilityStyles: " hiden ",
      });
    } else {
      this.setState({
        deleteHistoryDialogVisibilityStyles: null,
      });
    }
  }

  deleteHistory() {
    this.setState({ historyArray: [] }, () => {
      try {
        localStorage.setItem("localHistory", "[]");
      } catch (e) {
        console.error(e);
      }
    });
    this.showDeleteHistoryDialog();
  }

  render() {
    const historyPanelItemsList = (
      <div className="history_panel_dredatawarehouse">
        <div className="paper_dredatawarehouse_noborder">
          <div className="history_dredatawarehouse_header_sub">
            <div className="grid_container_dredatawarehouse">
              <p className="title_dredatawarehouse_left "> Query History </p>

              <button
                className="button_dredatawarehouse_right_altcolor"
                onClick={this.showDeleteHistoryDialog}
              >
                <FontAwesomeIcon icon={faTrash} size="2x" />{" "}
              </button>
              <button
                className="button_dredatawarehouse_right history_no_margin_left_dredatawarehouse"
                onClick={this.closeDrawer}
              >
                <FontAwesomeIcon icon={faTimes} size="2x" />{" "}
              </button>
            </div>
          </div>
          {/* Delete History Button and Dialog */}
          <div
            className={
              this.state.deleteHistoryDialogVisibilityStyles +
              " list_item_dredatawarehouse_list_header"
            }
          >
            <div className="history_dredatawarehouse_header_sub">
              <p className="subtitle_dredatawarehouse"> Delete history? </p>
            </div>
            <button
              className="button_dredatawarehouse_right_altcolor"
              onClick={this.deleteHistory}
            >
              Yes
            </button>
            <button
              className="button_dredatawarehouse_right_altcolor"
              onClick={this.showDeleteHistoryDialog}
            >
              No
            </button>
          </div>

          <hr />

          {/* History Items List */}
          {this.state.historyArray
            .slice(0)
            .reverse()
            .map((item) => {
              // Item[0] is the URL
              // Item[1] are the rules?

              // Display the current item iff it belongs to currently active db
              if (
                item[0].indexOf(lib.getDbConfig(this.props.dbIndex, "url")) >= 0
              ) {
                let tableName = this.extractTableNameFromURL(item[0]);
                if (tableName.length > displayLengthCutoff) {
                  tableName =
                    tableName.substring(0, displayLengthCutoff) + "...";
                }

                // If there are rules are present, then display it with limited number of rows for rules
                if (item[0] && item[1]) {
                  let rules = this.recursiveRulesExtraction(
                    item[1]["rules"],
                    item[1]["condition"],
                    0
                  );
                  let index = lib.elementPositionInArray(
                    item,
                    this.state.historyArray
                  );

                  // When user hovers over a history item, show rest of the lines
                  let classNames = " hiden ";
                  if (this.state.displayIndex === index) {
                    classNames = null;
                  }

                  return (
                    <div
                      className="list_item_dredatawarehouse button_history_dredatawarehouse"
                      key={index}
                      onMouseEnter={this.changeDisplayIndex.bind(this, index)}
                      onClick={this.handleHistoryItemClick.bind(this, index)}
                    >
                      <button
                        className="button_icon_history_dredatawarehouse"
                        onClick={this.handleLinkIconClick.bind(this, index)}
                      >
                        <FontAwesomeIcon icon={faShareAlt} size="2x" />{" "}
                      </button>

                      {/* Nicely formatted history item */}
                      <>
                        <div className="history_list_item_dredatawarehouse_list">
                          <p className="history_list_item_dredatawarehouse_text_primary">
                            {tableName}
                          </p>
                        </div>
                        {rules.map((rule) => {
                          let displayStr = "";
                          let columnName = "";
                          let displayName = "";
                          let rawOperator = "";
                          let niceOperator = "";
                          for (let i = 0; i < rule.length; i++) {
                            displayStr += " " + rule[i] + " ";
                            // if there are more than 1 rules (i.e. it's not AND/OR only) then extract column name
                            if (i === 1) {
                              columnName = rule[0].replace(/\s/g, "");
                              rawOperator = rule[1].replace(/\s/g, "");
                              niceOperator = lib.translateOperatorToHuman(
                                rawOperator
                              );
                            }
                          }

                          // find column's rename rules from config
                          if (columnName) {
                            let columnRename = lib.getColumnConfig(
                              this.props.dbIndex,
                              this.extractTableNameFromURL(item[0], true),
                              columnName,
                              "rename"
                            );
                            displayName = columnRename
                              ? columnRename
                              : columnName;
                          }

                          displayStr = displayStr
                            .replace(columnName, displayName)
                            .replace(rawOperator, niceOperator)
                            .replace(/\t/g, " . . ");
                          let currRuleIndexInRules = lib.elementPositionInArray(
                            rule,
                            rules
                          );

                          if (displayStr.length > displayLengthCutoff) {
                            displayStr =
                              displayStr.substring(0, displayLengthCutoff) +
                              "...";
                          }

                          return (
                            <div
                              className={
                                currRuleIndexInRules > 3
                                  ? "history_list_item_dredatawarehouse_list " +
                                    classNames
                                  : "history_list_item_dredatawarehouse_list "
                              }
                            >
                              <p className="history_list_item_dredatawarehouse_text_secondary">
                                {displayStr}
                              </p>
                            </div>
                          );
                        })}
                      </>
                    </div>
                  );
                } else {
                  // If only table name is present, then display just a table name
                  let index = lib.elementPositionInArray(
                    item,
                    this.state.historyArray
                  );

                  return (
                    <div
                      className="list_item_dredatawarehouse button_history_dredatawarehouse"
                      key={index}
                      onMouseEnter={this.changeDisplayIndex.bind(this, index)}
                      onClick={this.handleHistoryItemClick.bind(this, index)}
                    >
                      <button
                        className="button_icon_history_dredatawarehouse"
                        onClick={this.handleLinkIconClick.bind(this, index)}
                      >
                        <FontAwesomeIcon icon={faShareAlt} size="2x" />{" "}
                      </button>

                      <>
                        <div className="history_list_item_dredatawarehouse_list">
                          <p className="history_list_item_dredatawarehouse_text_primary">
                            {tableName}
                          </p>
                        </div>
                        <div
                          className={"history_list_item_dredatawarehouse_list "}
                        >
                          <p className="history_list_item_dredatawarehouse_text_secondary">
                            {"Get random rows..."}
                          </p>
                        </div>
                      </>
                    </div>
                  );
                }
              } else {
                return null;
              }
            })}
        </div>
        <Snackbar
          open={this.state.snackBarVisibility}
          onClose={() => {
            this.setState({ snackBarVisibility: false });
          }}
          message={<span id="message-id">{this.state.snackBarMessage}</span>}
        />
      </div>
    );

    return (
      <Drawer
        open={this.props.historyPaneVisibility}
        onClose={this.closeDrawer}
        elements={historyPanelItemsList}
      />
    );
  }
}

const Drawer = (props) => {
  if (props.open) {
    return (
      <div className="drawer_right_dredatawarehouse">{props.elements}</div>
    );
  } else {
    return null;
  }
};

const Snackbar = (props) => {
  if (props.open) {
    return (
      <div className="snackbar_dredatawarehouse">
        <p>{props.message}</p>
        <button onClick={props.onClose}>
          {" "}
          <FontAwesomeIcon icon={faTimes} size="2x" />{" "}
        </button>
      </div>
    );
  } else {
    return null;
  }
};
