import React, { Component } from "react";
import { Multiselect } from "multiselect-react-dropdown";
import "./styles.css";
import LoginDialog from "./LoginDialog";

let lib = require("../utils/library");

export default class DbPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: undefined,
      open: false,
      databases: [],
      isLoginFdpOpen: null,
      loginDialogOpen: false,
    };
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
  }

  handleLoginButtonClick = () => {
    if (this.props.isLoggedIn) {
      // logout the user
      this.props.handleLogoutClick();
    } else {
      this.setState({
        loginDialogOpen: !this.state.loginDialogOpen,
      });
    }
  };

  handleLoginDialogCloseClick = () => {
    this.setState({
      loginDialogOpen: false,
    });
  };

  handleClickListItem = (event) => {
    this.setState({ open: true, anchorEl: event.currentTarget });
  };

  handleMenuItemClick(optionsList, index) {
    this.setState({ open: false });
    if (this.props.dbIndex !== this.state.databases.indexOf(index)) {
      this.props.changeDbIndex(this.state.databases.indexOf(index));
    }
  }

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  // get a list of databases in the config.json
  componentDidMount() {
    let databasesMapped = [];
    lib
      .getValueFromConfig("databases")
      .map(
        (obj, index) =>
          (databasesMapped[index] = obj.title || "Untitled database")
      );
    this.setState({
      databases: databasesMapped,
    });
  }

  componentWillReceiveProps(newProps) {
    if (
      (newProps.publicDBStatus === "private" ||
        newProps.publicDBStatus === "read") &&
      !newProps.isLoggedIn
    ) {
      if (this.state.isLoginFdpOpen === null) {
        this.setState({
          isLoginFdpOpen: true,
        });
      }
    }
  }

  render() {
    var style_container = {
      multiselectContainer: {
        // "height": "500px"
      },
      searchBox: {
        border: "none",
        borderBottom: "1px solid grey",
        borderRadius: "0px",
        fontSize: "10px",
        height: "15px",
      },
      optionListContainer: {
        height: "500px !important",
      },
      inputField: {
        margin: "5px",
        fontSize: "10px",
        fontColor: "black",
      },
      optionContainer: {
        border: "1px solid grey",
      },
      option: {
        color: "black",
        fontSize: "15px",
        fontWeight: "400",
      },
      "option:hover": {
        backgroundColor: "rgb(155,125,64)",
        color: "white",
      },
      highlight: {
        color: "white",
        backgroundColor: "rgb(155,155,155)",
      },
      "highlight::hover": {
        backgroundColor: "rgb(155,125,64)",
        color: "white",
      },
      groupHeading: {
        color: "rgb(155,125,64)",
      },
    };

    let dbTitle =
      lib.getDbConfig(this.props.dbIndex, "title") || "Untitled database";
    return (
      <>
        <div className="paper_dredatawarehouse_noborder">
          <div className="grid_container_dredatawarehouse">
            <p className="title_dredatawarehouse_left">Choose a Database</p>
            <button
              className="button_dredatawarehouse_right_altcolor_login"
              onClick={() => {
                this.handleLoginButtonClick();
              }}
            >
              {this.props.isLoggedIn ? "Logout" : "Login"}
            </button>
          </div>
          <div className="radio_dredatawarehouse">
            <Multiselect
              id="lock-menu"
              anchorEl={this.state.anchorEl}
              singleSelect={true}
              isObject={false}
              // selectedValues={this.state.columnChosen}
              options={this.state.databases}
              // showCheckbox={false}
              onSelect={this.handleMenuItemClick}
              avoidHighlightFirstOption={true}
              // closeIcon="cancel"
              placeholder={this.state.databases[this.props.dbIndex]}
              style={style_container}
            />
          </div>
        </div>
        <LoginDialog
          dbName={dbTitle.replace("Database", "db").replace("database", "db")}
          setUserEmailPassword={this.props.setUserEmailPassword}
          open={this.state.loginDialogOpen}
          handleLoginDialogCloseClick={this.handleLoginDialogCloseClick}
          SingleSignOn={this.props.SingleSignOn}
        />
      </>
    );
  }
}
